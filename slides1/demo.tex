\documentclass[10pt, compress]{beamer}
% \usetheme[titleprogressbar]{m}
\usepackage[utf8]{inputenc}
\usepackage[russian,english]{babel}
\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
% \usepackage{minted}

% \usepgfplotslibrary{dateplot}

% \usemintedstyle{trac}

\title{High dimensional and spatial analyses for~linguistics}
\subtitle{Dimensionality reduction}
\author{Dmitry Nikolaev}
\institute{Dynamics of Language Lab\\The Hebrew University of Jerusalem}
\date{}

\begin{document}

\maketitle

\begin{frame}[fragile]
\frametitle{Contents of the lecture}
\begin{enumerate}
  \item Visualising high-dimensional data directly
  \item PCA
  \begin{enumerate}
    \item Algorithm
    \item PCA as a visualisation tool
    \item Interpreting PCA
    \item PCA: pro et contra
  \end{enumerate}
  \item MDS
  \item Autoencoders
  \item t-SNE
\end{enumerate}
\end{frame}

\begin{frame}[fragile]
\frametitle{CLICS data}

\begin{table}[]
\centering
\begin{tabular}{@{}llllll@{}}
\toprule
Language & Verb & hear & feel & know & listen \\ \midrule
pui\_std & {[}hui{]} & 1 & 0 & 0 & 0 \\
cao\_std & {[}nika{]} & 1 & 0 & 0 & 1 \\
jpn\_std & {[}kiku{]} & 1 & 0 & 0 & 1 \\
che\_Akkin & {[}\foreignlanguage{russian}{хаз}{]} & 1 & 0 & 0 & 0 \\
che\_std & {[}\foreignlanguage{russian}{хаза}{]} & 1 & 0 & 0 & 0 \\
yad\_std & {[}tuw\~{a}c\v{u}{]} & 1 & 0 & 0 & 1 \\ \bottomrule
\end{tabular}
\end{table}
\end{frame}

\begin{frame}[fragile]
  \frametitle{A scatterplot matrix}
\begin{figure}
 \centering
 \includegraphics[width=\textwidth]{CLICS_scatterplot_matrix.png}
\end{figure}
\end{frame}

\begin{frame}[fragile]
  \frametitle{A scatterplot matrix: code}

\begin{verbatim}
# Import data and convert it to a numeric matrix
d_meta <- read.csv('clics_updated.csv', h = T, sep = '\t')
d <- read.csv('clics_matrix_cleaned.csv', h = T, sep = '\t')
d_m <- as.matrix( d[,3:19] )
rownames(d_m) <- paste(d$X, d$X.1)
colnames(d_m) <- colnames(d)[3:19]

# Draw a scatterplot matrix
# All values are either 0 or 1
# need jitter to see the cloud
pairs(jitter(d_m))
\end{verbatim}

\end{frame}

\begin{frame}[fragile]
  \frametitle{A corrplot}
\begin{figure}
 \centering
 \includegraphics[width=\textwidth]{corrplots.pdf}
\end{figure}
\end{frame}

\begin{frame}[fragile]
  \frametitle{A corrplot with significane testing}
\begin{figure}
 \centering
 \includegraphics[width=\textwidth]{corrplots_p_values.pdf}
\end{figure}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Corrplots: code}

\begin{verbatim}
library(corrplot)
library(Hmisc) # The import for rcorr function

corrplot(cor(d_m), type = 'upper', diag = F)

d_m_corr <- rcorr(d_m)
corrplot(cor(d_m), type = 'upper',
         diag = F, p.mat = d_m_corr$P)
\end{verbatim}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Simple heatmap}
\begin{figure}
 \centering
 \includegraphics[width=\textwidth]{heatmap_unsorted.pdf}
\end{figure}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Heatmap with reordering}
\begin{figure}
 \centering
 \includegraphics[width=\textwidth]{clicks_heatmap.pdf}
\end{figure}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Consonant heatmap}
\begin{figure}
 \centering
 \includegraphics[height=0.8\textheight]{consonant_heatmap_crop.pdf}
\end{figure}
\end{frame}

\begin{frame}[fragile]\frametitle{Heatmaps: code}

\begin{verbatim}
# An in-built R function
heatmap(d_m, cexRow = 0.2, margins = c(10,5))

# A cleaner heatmap with heatmap.2
library(gplots)
heatmap.2(d_m, dendrogram = 'none', key = F,
          trace = 'none', lwid = c(0.15, 5),
          lhei = c(0.15, 5), margins = c(10,8),
          cexRow = 0.3, col = c('orange', 'red'))
\end{verbatim}


\end{frame}

\begin{frame}[fragile]
  \frametitle{Principal component analysis}
The oldest method for dimensionality reduction, rediscovered several times in the early 20th~c.

The algorithm:

Repeat $n-1$ times:

Choose the axis going through the point cloud such that
\begin{enumerate}
  \item It is orthogonal to all previously selected axes.
  \item The variance along that axis is maximal.
\end{enumerate}

Data values should be \textit{scaled}, i.e. have their mean $= 0$ and standard deviation $= 1$.
\end{frame}

\begin{frame}[fragile]
\frametitle{Principal component analysis: 2d case}
\begin{figure}
  \centering
 \includegraphics[height=0.8\textheight]{2d_PCA.png}
\end{figure}
{\footnotesize Wikimedia Commons}
\end{frame}

\begin{frame}[fragile]
\frametitle{Principal component analysis: 2d case}
\begin{figure}
  \centering
 \includegraphics[width=\textwidth]{2d_PCA_labels.png}
\end{figure}
{\footnotesize Wikimedia Commons}
\end{frame}

\begin{frame}[fragile]
  \frametitle{PCA on CLICS data, only the first component}
\begin{figure}
  \centering
 \includegraphics[width=\textwidth]{stripchart.pdf}
\end{figure}
\end{frame}

\begin{frame}[fragile]\frametitle{PCA: code}

{\small
\begin{verbatim}
d_scaled <- scale(d_m)
pc_d <- princomp(d_scaled) # Older method
pc_d2 <- prcomp(d_scaled) # Preferred method

# Leave only one component
# There is a stripchart function in the standarb lib
# but it cannot colour the points based on a factor
library(ggplot2)
ggplot(aes(x = fst), data = data.frame(fst = pc_d$scores[,1])) +
    geom_dotplot(fill = heat.colors(2)[1+d$hear], dotsize=.3,
                 binwidth=.3, stackdir = 'center', stackratio=.7) +
    scale_y_continuous(NULL, breaks = NULL) +
    theme_bw()
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
  \frametitle{PCA on CLICS data, the first two components}
\begin{figure}
  \centering
 \includegraphics[width=\textwidth]{pca_2d_hear_see.pdf}
\end{figure}
\end{frame}

\begin{frame}[fragile]\frametitle{2-component scatterplot: code}
    
\begin{verbatim}
plot(pc_d$scores[,2] ~ pc_d$scores[,1],
     pch = 16+d$see,
     col = (d$hear+1),
     cex = 1.5,
     xlab = 'First principal component',
     ylab = 'Second principal component')
\end{verbatim}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Variance explained by components}
\begin{figure}
  \centering
 \includegraphics[width=\textwidth]{pca_variance_explained.pdf}
\end{figure}
\end{frame}

\begin{frame}[fragile]\frametitle{Variance analysis: code}
    
\begin{verbatim}
# Can compute variance shares directly
eigs <- pc_d$sdev^2
variance_shares <- eigs / sum(eigs)

# Or just look at the screeplot
screeplot(pc_d, type = 'l', main = '')
\end{verbatim}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Verbs' connections to the first and second PC, a.k.a loadings}
\begin{figure}
  \centering
 \includegraphics[width=\textwidth]{pca_loadings.pdf}
\end{figure}
\end{frame}

\begin{frame}[fragile]\frametitle{Extracting loadings: code}
    
\begin{verbatim}
par(mfrow = c(2,1))
barplot(sort(pc_d$loadings[,1], decreasing=T))
barplot(sort(pc_d$loadings[,2], decreasing=T))
\end{verbatim}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Combining everything into a biplot}
\begin{figure}
  \centering
 \includegraphics[height=0.8\textheight]{pca_biplot.pdf}
\end{figure}
\end{frame}

\begin{frame}[fragile]\frametitle{Biplot code}
    
\begin{verbatim}
# Omit the `rep' argument if you want the
# rownames. Too crowded in our case
biplot(pc_d, xlabs = rep('*', nrow(d)))
\end{verbatim}

\end{frame}

\begin{frame}[fragile]
  \frametitle{No good reason not to look at other components}
\begin{figure}
  \centering
 \includegraphics[width=\textwidth]{pca_pairs.pdf}
\end{figure}
\end{frame}

\begin{frame}[fragile]
  \frametitle{A biplot of the 1st and 3rd components}
\begin{figure}
  \centering
 \includegraphics[height=0.8\textheight]{pca_biplot_first_third.pdf}
\end{figure}
\end{frame}

\begin{frame}[fragile]\frametitle{Biplot with choices}
    
\begin{verbatim}
biplot(pc_d,
       choices = c(1,3),           # <---
       xlabs = rep('*', nrow(d)))
\end{verbatim}

\end{frame}

\begin{frame}[fragile]
  \frametitle{A non-linguistic example}
\begin{figure}
  \centering
 \includegraphics[width=\textwidth]{PCA_analysis_history.png}
\end{figure}

{\footnotesize \textit{Turchin et al.} Quantitative historical analysis uncovers a single dimension of complexity that structures global variation in human social organization. 10.1073/pnas.1708800115}

\end{frame}

\begin{frame}[fragile]
  \frametitle{PCA: pro et contra}
PRO:
\begin{enumerate}
  \item PCA is highly interpretable and gives a lot of information about the data.
  \item It is relatively easy to compute. Any decent linear algebra package is enough. The implementation in \texttt{R} (\texttt{prcomp} and \texttt{princomp} functions) is very nice.
  \item PCA is amenable to bootstrapping so we can provide confidence intervals for the estimates.
\end{enumerate}
\end{frame}

\begin{frame}[fragile]
  \frametitle{PCA: pro et contra}
CONTRA:

PCA is based on linear operations (translation, rotation, and scaling). Not all data are linear:

\vspace{10pt}

\begin{figure}
  \centering
 \includegraphics[width=.8\textwidth]{SOMsPCA.PNG}
\end{figure}

{\footnotesize Wikimedia Commons}

\end{frame}

\begin{frame}[fragile]
\frametitle{PCA: pro et contra}
CONTRA:

\vspace{5pt}

PCA cannot deal with datasets with more variables than observations.

\end{frame}

\begin{frame}[fragile]
  \frametitle{From PCA to MDS and beyond---focus on distances}
\begin{figure}
  \centering
 \includegraphics[width=.8\textwidth]{MDSvPCA.png}
\end{figure}

{\footnotesize ttnphns at CrossValidated}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Multidimensional scaling}

MDS is a family of methods, which, given a dissimilarity matrix, try to embed the points in a given number of dimensions and keep the distances between points more or less the same by minimising some \textit{stress function}. (Often people write, `We used MDS', which is uninformative---should specify the particular method.)\pause

\vspace{5pt}

Classical (a.k.a. \textit{metric}) MDS assumes Euclidean distances in the original space and is very similar in its implementation to PCA, but there are more versatile and powerful methods (commonly called \textit{non-metric MDS}), which only assume monotonic relationship (bigger distances in the embedding correspond to bigger original distances).

\end{frame}

\begin{frame}[fragile]
  \frametitle{Kruskal's MDS of CLICS data}

\begin{figure}
  \centering
 \includegraphics[width=\textwidth]{mds_rgb.pdf}
\end{figure}

\end{frame}

\begin{frame}[fragile]\frametitle{MDS code}
{\small
\begin{verbatim}
library(MASS)    # For isoMDS
library(cluster) # For distance measures

d_dist <- dist(jitter(d_m), method = 'manhattan')
d_mds <- isoMDS(d_dist)

plot(d_mds$points[,2] ~ d_mds$points[,1],
     pch = 16 - 15*d$understand,
     col = 3 + d$hear - d$see, # We can use the face that `hear'
                               # and `see' don't overlap
     cex = 1.3, add = T, xlab = '', ylab = '')
legend('topright', c('see', 'neither', 'hear'), pch = rep(16,3),
       col = c('red', 'green3', 'blue'))
legend('topleft', c('+understand', '-understand'), pch = c(1,16))
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]\frametitle{Distance metrics}
\begin{figure}
\begin{minipage}{.5\textwidth}
Two most popular distance metrics:
\begin{enumerate}
  \item Eulidean distance: $\sqrt{\displaystyle\sum_{i=1}^{n}\left(q_i - p_i\right)^{2}}$
  \item Manhattan (a.k.a. taxicab) distance: $\displaystyle\sum_{i=1}^{n}\left|q_i - p_i\right|^{2}$
\end{enumerate}
\end{minipage}%
\begin{minipage}{.5\textwidth}
\centering
\includegraphics[height=0.4\textheight]{distance_metrics.pdf}  
\end{minipage}%
\end{figure}
\end{frame}

\begin{frame}[fragile]\frametitle{A popular choice for tasks involving strings of letters---Levenshtein distance a.k.a. edit distance}
    
\begin{figure}
  \centering
 \includegraphics[width=.9\textwidth]{levenshtein.jpg}
\end{figure}

{\footnotesize Hector Franco. \url{https://www.slideshare.net/hecfran/tree-distance-algorithm}}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Spatial MDS (five-dimensional embedding)}

Given a set of distances between points equipped with geographical coordinates

\begin{enumerate}
  \item Embed the original data in the $3$-D space.
  \item Intepret the MDS axes as RGB values and colour the original data points accordingly.
\end{enumerate}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Heeringa 2004 (Levenshtein distances between dialects)}

\begin{figure}
  \centering
 \includegraphics[height=.7\textheight]{Heeringa_MDS.png}
\end{figure}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Donohue \& Kalyan 2017 (based on morphosyntax features in the World Phonotactics Database)}

\begin{figure}
  \centering
 \includegraphics[width=\textwidth]{morphosyntactic_distances.pdf}
\end{figure}

{\small ``Individual colours are not directly interpretable; however, warm colours (red, yellow etc.) correspond to predominantly head-initial languages, and cool colours (blue, green etc.) correspond to predominantly head-final languages.'' (\url{https://www.academia.edu/31887097/Map_of_morphosyntactic_distances_among_the_world_s_languages})}

\end{frame}

\begin{frame}[fragile]\frametitle{From MDS to manifold learning}
    
Today MDS is often regarded as one of the methods for \textit{manifold learning}. The objective of manifold learning is to find a subset of the original multidimensional space so that

\begin{enumerate}
  \item It contained all the data points.
  \item It could be well described by $k$ coordinates.\pause
\end{enumerate}

Two existing approaches are to find a matrix of distances \textit{along the manifold} a.k.a. geodesic distances (focus on global structure) or to assume that the subspace is \textit{localy linear}, so we can divide into many pieces and then arrange them in some order on the $2-$D plane (focus on local structure).

\end{frame}

\begin{frame}[fragile]\frametitle{From MDS to manifold learning}

Cf. \textit{isomap} (focus on global structure) and \textit{lle} (focus on local structure):

\begin{figure}
  \centering
 \includegraphics[width=\textwidth]{manifold_learning_comparison.png}
\end{figure}

{\footnotesize \url{http://scikit-learn.org/}}

\end{frame}

\begin{frame}[fragile]\frametitle{Autoencoders}
    
Autoencoder is a neural network that, in the basic approach, is trained to reconstruct its input from a compressed representation:

\vspace{5pt}

\begin{figure}
  \centering
 \includegraphics[width=\textwidth]{Autoencoder-architecture.png}
\end{figure}

{\footnotesize Ahmed et al. 2018. Intelligent condition monitoring method for bearing faults from highly compressed measurements using sparse over-complete features. 10.1016/j.ymssp.2017.06.027}

\end{frame}

\begin{frame}[fragile]\frametitle{Why autoencoders}

PRO:

Neural networks are the most powerful function-approximation / structure-recovery mechanism known to man today. With a right architecture and enough data they beat all other methods.

\vspace{5pt}

CONTRA:

They need lots of data, and powerful architectures may be quite complicated. One needs to have a lot of technical experience to use NNs efficiently.
\end{frame}

\begin{frame}[fragile]\frametitle{A stacked autoencoder}
    
\begin{figure}
  \centering
 \includegraphics[width=\textwidth]{stacked_autoencoder.png}
\end{figure}

{\footnotesize Berniker \& Kording 2015. Deep networks for motor control functions. 10.3389/fncom.2015.00032}
\end{frame}


\begin{frame}[fragile]\frametitle{The famous success story}
    
\begin{figure}
  \centering
 \includegraphics[height=.7\textheight]{Hinton_autoencoder.png}
\end{figure}

{\footnotesize Hinton \& Salakhutdinov. Reducing the Dimensionality of
Data with Neural Networks. 10.1126/science.1127647}
\end{frame}

\begin{frame}[fragile]\frametitle{t-SNE}
    
One of the most widely used and ofren recommended non-neural solutions for dimensionality reduction today.

\vspace{5pt}

Originally presented in \textit{L.J.P. van der Maaten and G.E. Hinton.} Visualizing High-Dimensional Data Using {t-SNE}. Journal of Machine Learning Research 9(Nov):2579--2605, 2008.

\end{frame}

\begin{frame}[fragile]\frametitle{Key features of t-SNE}
    
Instead of finding an optimal new dissimilarity matrix for the points on a low-dimensional surface, the method finds a \textit{probability distribution} of points being sampled together from a low-dimensional surface that will match the probability distribution defined on the original data.\pause

\vspace{5pt}

This search entails optimising a non-convex function, which is done by trying gradient descent from different starting points. It is possible that different runs of {t-SNE} will produce different results (although in my experience the difference is negligible).\pause

\vspace{5pt}

Simple manifolds can be better tackled by other methods, but on really complex data the results are often sublime.

\end{frame}

\begin{frame}[fragile]\frametitle{Handwritten digit classification}
    
\begin{figure}
  \centering
  \includegraphics[height=0.7\textheight]{mnist_tsne.jpg}
\end{figure}

{\footnotesize https://lvdmaaten.github.io/tsne/}

\end{frame}

\begin{frame}[fragile]\frametitle{General image classification}
    
\begin{figure}
  \centering
  \includegraphics[height=0.7\textheight]{caltech101_tsne.jpg}
\end{figure}

{\footnotesize https://lvdmaaten.github.io/tsne/}

\end{frame}

\begin{frame}[fragile]\frametitle{CLICS data}
    
\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{tsne_rgb.pdf}
\end{figure}

\end{frame}

\begin{frame}[fragile]\frametitle{CLICS Rtsne: code}
  
{\small  
\begin{verbatim}
library(Rtsne)
# There is another t-SNE package called `tsne'
# It seems to be poorly implemented
d_m_tsne <- Rtsne(d_m) # Will not work: there are duplicates
# Possible solutions:        
# 1. Remove duplicates (and lose data)           
# 2. Add noise (is ok for most cases)
# 3. Go to distance matrix directly
d_m_tsne <- Rtsne(d_dist) # This works
plot(d_m_tsne$Y, pch = 16 - 15*d$understand,
     col = 3 + d$hear - d$see, cex = 1.3,
     xlab = '', ylab = '')
legend('topright', c('see', 'neither', 'hear'), pch = rep(16,3),
       col = c('red', 'green3', 'blue'))
legend('bottomleft', c('+understand', '-understand'), pch = c(1,16))
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]\frametitle{Further analysis}
    
Let's do PCA on the non-aligned guys:

\begin{figure}
  \centering
  \includegraphics[height=.7\textheight]{pca_nonaligned.pdf}
\end{figure}

{\small It seems that the opposition `listen' v. `look at' is the culprit}

\end{frame}

\begin{frame}[fragile]\frametitle{More PCA code}

\begin{verbatim}
d_res <- d[ (d$see == 0) & (d$hear == 0), ]
d_res <- d_res[,-c(3, 18)]
d_res_m <- as.matrix(d_res[,3:17])
rownames(d_res_m) <- paste(d$X, d$X.1)[(d$see == 0) &
                                       (d$hear == 0)]
colnames(d_res_m) <- colnames(d_res[3:17])
# Remove zero columns      
d_res_m <- d_res_m[,which(colSums(d_res_m) != 0)]
res_pca <- prcomp(scale(d_res_m))
biplot(res_pca, xlabs = rep('*', nrow(d_res_m)))
\end{verbatim}

\end{frame}

\begin{frame}[fragile]\frametitle{Relabeling the t-SNE plot}
    
\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{tsne_rgb_full.pdf}
\end{figure}

\end{frame}

\begin{frame}[fragile]\frametitle{Relabeling: code}
    
{\small
\begin{verbatim}
plot(d_m_tsne$Y, pch = 16 - 15*d$understand + 
                            d$listen - d$look..look.at,
     col = 3 + d$hear - d$see, cex = 1.3,
     xlab = '', ylab = '')
legend('topright', c('see', 'neither', 'hear'), pch = rep(16,3),
       col = c('red', 'green3', 'blue'))
legend('bottomleft', c('+understand', '-understand',
                       'listen', 'look at'),
       pch = c(1,16, 17, 15))
\end{verbatim}
}

\end{frame}


\end{document}