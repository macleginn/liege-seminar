\documentclass[10pt, compress]{beamer}

\usepackage[utf8]{inputenc}
\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{booktabs}
\usepackage{xcolor}
\usepackage{listings}

\title{High dimensional and spatial analyses for~linguistics}
\subtitle{Going spatial}
\author{Dmitry Nikolaev}
\institute{Dynamics of Language Lab\\The Hebrew University of Jerusalem}
\date{}

\begin{document}

\maketitle

\begin{frame}[fragile]\frametitle{Overview}
    
\tableofcontents

\end{frame}

\section{Different ways of doing spatial analysis}

\begin{frame}[fragile]\frametitle{Some of ways of doing spatial analysis in linguistics}
    
\begin{enumerate}
  \item Analysis of variation inside and across (macro)-regions.\pause
  \item Regression on distances (from a certain point or pairwise distances).\pause
  \item Using extra-linguistic variables (altitude, air humidity, sexual mores, frequencies of genetic haplogroups).\pause
  \item Using neighbour graphs.
\end{enumerate}

\end{frame}

\section{Comparing macro-regions}

\begin{frame}[fragile]\frametitle{Adding macro-region labels to CLICS data}
    
Doesn't help straight away:

\begin{figure}
\centering
\includegraphics[width=.9\textwidth]{tsne_w_areas.pdf}
\end{figure}

{\small Colour coded by phylum.}

\end{frame}

\begin{frame}[fragile]\frametitle{Automated colouring and legends}

{\small    
\begin{verbatim}
# rainbow(n) creates a vector of colours of length `n',
# which we can then index by a factor
new_colors <- rainbow(length(levels(d_w_meta$phylum)))
plot(d_m_tsne$Y, cex = 2,
col = alpha(new_colors[d_w_meta$phylum], 0.5),
       pch = as.integer(d_w_meta$area)+15)
# Extract names of areas and show which
# points were used for them
legend('topright', levels(d_w_meta$area),
       pch = seq_along(levels(d_w_meta$area))+15)
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]\frametitle{By-area presentation}
    
\begin{figure}
\centering
\includegraphics[width=\textwidth]{tsne_by_area.pdf}
\end{figure}

{\small Eurasia and South America seem to be worth a closer look.}

\end{frame}

\begin{frame}[fragile]\frametitle{Is where ggplot2 shines}
    
\begin{verbatim}
library(ggplot2)

# ggplot2 needs a data.frame
tsne.df <- as.data.frame(d_m_tsne$Y)

ggplot(aes(x = V1, y = V2, colour = d_w_meta$phylum),
       data = tsne.df) +
    geom_point(size = 2) +
    # To add a title to the legend
    scale_colour_discrete(name = "Phylum") + 
    facet_wrap(~ d_w_meta$area) +              # <---
    theme_bw() + xlab('') + ylab('')
\end{verbatim}

\end{frame}

\begin{frame}[fragile]\frametitle{Spot the difference}
    
\begin{figure}
\centering
\includegraphics[width=\textwidth]{obey_by_area.pdf}
\end{figure}

\end{frame}

\begin{frame}[fragile]\frametitle{We can compare correlation matrices}
    
\begin{figure}
\centering
\includegraphics[width=\textwidth]{eur_sam_cor.pdf}
\end{figure}

{\small But that is really inconvenient}

\end{frame}

\begin{frame}[fragile]\frametitle{Instead we can directly look at differences}
    
\begin{figure}
\centering
\includegraphics[width=\textwidth]{diff_cor.pdf}
\end{figure}

\end{frame}

\begin{frame}[fragile]\frametitle{Modified corrplots: code}
    
{\small
\begin{verbatim}
library(corrplot)
# Prepare data
eur.m <- d_m[ d_w_meta$area == 'Eurasia', ]
sam.m <- d_m[ d_w_meta$area == 'South America', ]
eur.cor <- cor(eur.m)
sam.cor <- cor(sam.m)

par(mfrow = c(1,2))
corrplot(eur.cor, type = "upper", diag = F,
         number.digits = 2, addCoef.col = 'black',
         method = 'color')
corrplot(sam.cor, type = "upper", diag = F,
         number.digits = 2, addCoef.col = 'black',
         method = 'color')
par(mfrow = c(1,1))

corrplot(eur.cor - sam.cor, type = "upper",
         diag = F, number.digits = 2, addCoef.col = 'black',
         method = 'color')
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]\frametitle{Correlations as distances}
    
Correlations are a bad distance measure because they range from $-1$ to $1$.\pause

\vspace{5pt}

However, it may be shown that if the vectors have mean $1$ and standard deviation of $0$, correlations between them are scaled squared Euclidean distances.\pause

\vspace{5pt}

Therefore, we can scale the verb vectors, compute squared Euclidean distances between them, and then use t-SNE or whatever to reduce the dimensionality.

\end{frame}

\begin{frame}[fragile]\frametitle{t-SNE of distances between verbs}
    
\begin{figure}
\centering
\includegraphics[width=\textwidth]{tsne_on_meanings_eur_v_sam.pdf}
\end{figure}

\end{frame}

\begin{frame}[fragile]\frametitle{The code I}
    
{\small
\begin{verbatim}
# Transpose the matrix to work with columns
eur.scaled.dist <- dist(t(scale(eur.m)), method = 'euclidean')
eur.scaled.dist <- eur.scaled.dist^2
sam.m <- sam.m[,which(colSums(sam.m) != 0)] # It's impossible to scale
                                            # a constant vector!
sam.scaled.dist <- dist(t(scale(sam.m)), method = 'euclidean')
sam.scaled.dist <- sam.scaled.dist^2

eur.scaled.tsne <- Rtsne(eur.scaled.dist, perplexity = 5)
eur.scaled.df <- as.data.frame(eur.scaled.tsne$Y)
eur.scaled.df$area <- 'Eurasia'

sam.scaled.tsne <- Rtsne(sam.scaled.dist, perplexity = 4)
sam.scaled.df <- as.data.frame(sam.scaled.tsne$Y)
sam.scaled.df$area <- 'South America'

all.scaled.df <- rbind(eur.scaled.df, sam.scaled.df)
all.scaled.df$label <- c(colnames(eur.m), colnames(sam.m))
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]\frametitle{The code II}
    
{\small
\begin{verbatim}
ggplot(data = all.scaled.df, aes(x = V1, y = V2)) +
    geom_text(label = all.scaled.df$label,
              cex = 6, colour = 'red') +
    facet_wrap(~ area) +
    theme_bw() + xlab('') + ylab('')
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]\frametitle{Modified WATP data}
    
{\small
\begin{table}[]
\centering
\begin{tabular}{@{}llllllll@{}}
\toprule
               & Koryak              & Mongolian & Swahili   & Matengo   \\
               & kory1246            & halh1238  & swah1253  & mate1258  \\
               & Chukotko-Kamchatkan & Mongolic  & Bantoid   & Bantoid   \\
               & 63.89771            & 48.32397  & -8.256052 & -11.55404 \\
               & 166.6947            & 106.2887  & 37.62403  & 35.0056   \\
               & N Asia              & C Asia    & Africa    & Africa    \\ \midrule
boil           & C                   & C         & C         & C         \\
finish         & L                   & C         & \fcolorbox{red}{white}{L/A}       & E         \\
dry            & C                   & C         & C         & S         \\
wake up        & C                   & C         & C         & E         \\
go out/put out & S                   & C         & A         & E         \\ \bottomrule
\end{tabular}
\end{table}
}

\end{frame}

\begin{frame}[fragile]\frametitle{Some statistics}
    
{\tiny
\begin{table}[]
\centerline{
\begin{tabular}{@{}lllllllllll@{}}
\toprule
Lang & Region & Phylum & Lat & Lon & \begin{tabular}[c]{@{}l@{}}Majority\\ types\end{tabular} & A/C ratio & \begin{tabular}[c]{@{}l@{}}Directed\\ derivation\end{tabular} & \begin{tabular}[c]{@{}l@{}}Derivation\\ vs rest\end{tabular} & \begin{tabular}[c]{@{}l@{}}Lexical\\ relatedness\end{tabular} & Lability \\ \midrule
Koryak & N Asia & \begin{tabular}[c]{@{}l@{}}Chukotko-\\ Kamchatkan\end{tabular} & 63.898 & 166.695 & C & 0.091 & 0.774 & 0.806 & 0.903 & 0.097 \\
Mongolian & C Asia & Mongolic & 48.324 & 106.289 & C & 0.24 & 0.886 & 0.943 & 0.971 & 0.029 \\
Swahili & Africa & Bantoid & 48.324 & 106.289 & C & 0.474 & 0.737 & 0.921 & 0.974 & 0.053 \\
Matengo & Africa & Bantoid & -8.256 & 37.624 & E & 1 & 0.276 & 0.828 & 0.862 & 0.034 \\
Herero & Africa & Bantoid & -8.256 & 37.624 & C & 0.2 & 0.562 & 0.875 & 0.938 & 0.062 \\
Georgian & SW Asia & Kartvelian & -11.554 & 35.006 & E & 2.2 & 0.5 & 0.938 & 0.938 & 0 \\
Lezgian & SW Asia & Lezgic & -21.023 & 20.566 & C & 0.667 & 0.645 & 0.839 & 1 & 0.161 \\
Even & N Asia & Tungusic & 41.816 & 44.319 & A & 2.5 & 0.519 & 0.667 & 0.778 & 0.111 \\
Manchu & N Asia & Tungusic & 41.816 & 44.319 & C & 0.045 & 0.639 & 0.667 & 0.75 & 0.083 \\
Nanai & N Asia & Tungusic & 41.516 & 47.895 & C & 0.9 & 0.528 & 0.722 & 0.806 & 0.083 \\
Udihe & N Asia & Tungusic & 70.669 & 130.914 & S & 0.2 & 0.343 & 0.4 & 0.514 & 0.086 \\ \bottomrule
\end{tabular}
}
\end{table}
}

\end{frame}

\begin{frame}[fragile]\frametitle{Majority types}
    
\begin{figure}
\centering
\includegraphics[width=\textwidth]{watp_majority_types_map.pdf}
\end{figure}

{\footnotesize The code is messy: had to write a custom function to implement mini-pie-charts. But in general plotting points on a map is easy.}

\end{frame}

\begin{frame}[fragile]\frametitle{Violin plots with medians and MADs}
    
\begin{figure}
\centering
\includegraphics[width=\textwidth]{stats_by_region.pdf}
\end{figure}

\end{frame}

\begin{frame}[fragile]\frametitle{Violin plots: code}
    
{\footnotesize
\begin{verbatim}
library(reshape)
scales.melt <- melt(scales[, !(colnames(scales) %in% c('A.C.ratio',
                                                       'Majority.types'))],
                    id = c('Lang', 'Region', 'Phylum', 'Lat', 'Lon'))
ggplot(data = scales.melt, aes(y = value, x = Region)) +
    geom_violin(trim = T, fill = 'lightblue') +
    stat_summary(fun.ymin = function(x) { median(x) - mad(x) },
                 fun.y = "median",
                 fun.ymax = function(x) { median(x) + mad(x) },
                 geom="pointrange", col = 'red') +
    theme_bw() + ylab('') +
    facet_wrap(~ variable)
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]\frametitle{A/C ratio has two outliers in Europe}
    
\begin{figure}
\centering
\includegraphics[width=\textwidth]{a_c_ratio_by_region.pdf}
\end{figure}

{\footnotesize Used \texttt{geom\_boxplot} instead of \texttt{geom\_violin}}

\end{frame}

\begin{frame}[fragile]\frametitle{Time for some significance testing}
    
Options for significance testing given non-normal data:

\begin{enumerate}
  \item Likelihood-ratio test. A powerful, but asymptotic test (good fit for large samples).
  \item Bootstrapping. Another asymptotic test, which is good for computing confidence intervals (in case of regression we don't want confidence intervals for coefficients of interest to include $0$).
  \item Permutation test. An exact test with very few assumptions. Good at computing $p$-values for small samples; doesn't give confidence intervals.
\end{enumerate}

\end{frame}

\begin{frame}[fragile]\frametitle{Likelihood-ratio test}
    
LR test compares a null model with a bigger model, which includes additional predictors.\pause

\vspace{5pt}

The likelihood ratio of two nested models given the data is $\chi^2$-distributed with degrees of freedom equal to the difference in the number of the parameters.\pause

\vspace{5pt}

For instance, if we a have a model with $1$ predictor and another model with $2$ additional predictors, we can compute the test statistic equal to $-2 \ln{\lambda}$, where $\lambda$ is the likelihood ratio, and the probability of obtaining a result of such or greater magnitude can be calculated using $\chi^2$ distribution with $2$ degrees of freedom.

\end{frame}

\begin{frame}[fragile]\frametitle{R code}
    
We can perform the calculations directly or just use the \texttt{lrtest} function:

\begin{verbatim}
library(lmtest)

lrtest(
  lm(A.C.ratio~Phylum, data = scales),
  lm(A.C.ratio~Phylum + Region, data = scales)
)
\end{verbatim}

\end{frame}

\begin{frame}[fragile]\frametitle{Results}
    
\begin{verbatim}
Likelihood ratio test

 Model 1: A.C.ratio ~ Phylum
 Model 2: A.C.ratio ~ Phylum + Region
   #Df  LogLik Df  Chisq Pr(>Chisq)                                                                                                                                                                                
 1  26 -192.94
 2  33 -172.00  7 41.878  5.488e-07 ***
 ---
 Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’
                 0.05 ‘.’ 0.1 ‘ ’ 1
\end{verbatim}

\end{frame}

\begin{frame}[fragile]\frametitle{The reverse test}
    
\texttt{A.C.ratio \textasciitilde~Region} vs \texttt{A.C.ratio \textasciitilde~Region + Phylum}:

\begin{verbatim}
Likelihood ratio test

 Model 1: A.C.ratio ~ Region
 Model 2: A.C.ratio ~ Phylum + Region
   #Df  LogLik Df  Chisq Pr(>Chisq)                                                                                                                                                                                
 1  10 -189.28
 2  33 -172.00 23 34.562    0.05745 .
\end{verbatim}

\end{frame}

\begin{frame}[fragile]\frametitle{Analogously for directed derivation}
    
\begin{verbatim}
Likelihood ratio test

 Model 1: Directed.derivation ~ Phylum
 Model 2: Directed.derivation ~ Phylum + Region
   #Df LogLik Df  Chisq Pr(>Chisq)

 1  26 33.749
 2  33 48.253  7 29.009  0.0001441 ***
 
 Model 1: Directed.derivation ~ Region
 Model 2: Directed.derivation ~ Region + Phylum
   #Df LogLik Df  Chisq Pr(>Chisq)                                                                                                     
 1  10 22.535
 2  33 48.253 23 51.437  0.0005948 ***
\end{verbatim}

\end{frame}

\begin{frame}[fragile]\frametitle{How to obtain between group differences?}

We could try to regress the variable of interest on phyla, taking the residuals from these models and do ANOVA on the. However, in this particular case this is not an ideal way to go.\pause

\vspace{5pt}

When performing analysis of such kind, we assume that phyla and regions are not connected in a causal fashion. This is of course not true, and when we `control' for phyla, we may distort the data (e.g., Indo-European and Uralic languages can have high A/C ration \textit{because} they are in Eurasia, but when we `account for phylum', we essentially demolish the effect we are interested in).

\end{frame}

\begin{frame}[fragile]\frametitle{A way out---by-phylum sampling}

The upper half of the table (which accidentally highlited an error in the original data):

{\tiny
\begin{verbatim}
          Austroasiatic Austronesian Bantoid Chukotko-Kamchatkan Cushitic
   Africa                0            0       3                   0        1
   C Asia                0            0       0                   0        0
   E Asia                0            0       0                   0        0
   Europe                0            0       0                   0        0
   N Asia                0            0       0                   1        0
   Papunesia             0            2       0                   0        0
   S Asia                0            0       0                   0        0
   SE Asia               1            1       0                   0        0
   SW Asia               0            0       0                   0        0

             Indo-European isolate Isolate Japonic Japonic  Kartvelian Koreanic
   Africa                0       0       0       0        0          0        0
   C Asia                1       0       0       0        0          0        0
   E Asia                0       0       1       3        1          0        1
   Europe               10       0       1       0        0          0        0
   N Asia                0       0       0       0        0          0        0
   Papunesia             0       0       0       0        0          0        0
   S Asia                8       1       0       0        0          0        0
   SE Asia               0       0       0       0        0          0        0
   SW Asia               1       0       0       0        0          1        0
\end{verbatim}
}

$34$ non-zero entries in all.

\end{frame}

\begin{frame}[fragile]\frametitle{Stratified sampling: code}

{\small
\begin{verbatim}
library(sampling)
num.samples <- sum(table(scales$Region, scales$Phylum) > 0)
# The function needs the data to be ordered by
# the stratification columns
s <- strata(scales[order(scales$Region,scales$Phylum),], 
            stratanames=c('Region', 'Phylum'),
            size = rep(1, num.samples)) # One data point for each
                                        # combination
strat.scales <- getdata(scales[order(scales$Region,scales$Phylum),],
                        s)
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]\frametitle{New violin plots}
    
\begin{figure}
\centering
\includegraphics[width=\textwidth]{stratified_violins.pdf}
\end{figure}

\end{frame}

\begin{frame}[fragile]\frametitle{Now we can do ANOVA}
    
In this case, we use Kruskal--Wallis test because the data, as is evident from the violin plots, are not normally distributed.

{\footnotesize
\begin{verbatim}
 > kruskal.test(strat.scales$A.C.ratio,
                strat.scales$Region)$p.value
 [1] 0.07409425
 
 > kruskal.test(strat.scales$Directed.derivation,
                strat.scales$Region)$p.value 
 [1] 0.1170976

 > kruskal.test(strat.scales$Derivation.vs.rest,
                strat.scales$Region)$p.value
 [1] 0.08148479
 
 > kruskal.test(strat.scales$Lexical.relatedness,
                strat.scales$Region)$p.value
 [1] 0.2134522

 > kruskal.test(strat.scales$Lability,
                strat.scales$Region)$p.value
 [1] 0.3556565
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]\frametitle{Where did the groove go?}
    
Why is nothing significant now even though the results of the LR test were significant?\pause

\vspace{5pt}

The problem is that now we have a smaller sample, and we use a rank-based test. Both of these factors reduce the \textit{power} of our test, i.e., make it more likely to accept the null hypothesis (of no difference) when it is not true.

\end{frame}

\begin{frame}[fragile]\frametitle{Remedies}
    
\begin{enumerate}
  \item Sample more phyla from each region.
  \item Target inter-regional phyla (Indo-European, Austronesian).
  \item Take larger regions (e.g., divide Eurasia into $2$ regions).
\end{enumerate}

\end{frame}

\begin{frame}[fragile]\frametitle{Return to exploration}
    
Let's look at how similar/different are individual verbs and languages with respect to transitivity derivations.

\vspace{5pt}

But what should we do with nominal vectors and values like `C/L'? 

\end{frame}

\begin{frame}[fragile]\frametitle{Jaccard similarity to the rescue}
    
A similarity metric for sets:

\[\operatorname{J}(A,B) = \dfrac{|A \cap B|}{|A \cup B|}\]

The corresponding dissimilarity metric: $1 - \operatorname{J}(A,B)$

\end{frame}

\begin{frame}[fragile]\frametitle{Dissimilarities between languages and verbs}
    
To compute the dissimilarity between a pair of \textit{languages}, sum all the pairwise differences between corresponding verbs in these two languages.\pause

\vspace{5pt}

To compute the dissimilarity between a pair of \textit{verbs}, sum all the pairwise differences between those two verbs in all languages.\pause

\vspace{5pt}

In both cases, we add pairwise differences between vector elements, so we can use the same code.

\end{frame}

\begin{frame}[fragile]\frametitle{Python code}

{\small 
\begin{verbatim}
import pandas as pd # A data-frame processing library for Python
import numpy as np  # A power mathematical library

def jaccard_metric(v1, v2):
    null_count = 0
    diffs = []
    for i in range(len(v1)):
        if pd.isnull(v1.iloc[i]) or pd.isnull(v2.iloc[i]):
            null_count += 1
        else:
            vals1 = set(v1.iloc[i].strip().split('/'))
            vals2 = set(v2.iloc[i].strip().split('/'))
            common = set.intersection(vals1, vals2)
            total = set.union(vals1, vals2)
            diffs.append(
                1 - len(common)/len(total)
            )
    return sum(diffs) + sum(diffs)/len(diffs) * null_count
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]\frametitle{Distance matrices}

{\footnotesize    
\begin{verbatim}
distance_matrix_langs = np.zeros((data.shape[1]-1,data.shape[1]-1))
for i in range(1, data.shape[1]):
    for j in range(1, data.shape[1]):
        if i != j:
            distance_matrix_langs[i-1,j-1] = jaccard_metric(data.iloc[5:,i], 
                                                            data.iloc[5:,j])
distance_matrix_verbs = np.zeros((data.shape[0]-5,data.shape[0]-5))
for i in range(5, data.shape[0]):
    for j in range(5, data.shape[0]):
        if i != j:
            distance_matrix_verbs[i-5,j-5] = jaccard_metric(data.iloc[i,1:], 
                                                            data.iloc[j,1:])
distance_verb_pd = pd.DataFrame(distance_matrix_verbs)
distance_verb_pd.columns = data.iloc[5:,0]
distance_verb_pd.index = data.iloc[5:,0]
distance_verb_pd.to_csv('verb_distance_matrix.csv')
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]\frametitle{The results}

\begin{figure}
\centerline{
\includegraphics[width=1.2\textwidth]{watp_tsne.pdf}
}
\end{figure}

\end{frame}

\section{Incorporating distances}

\begin{frame}[fragile]\frametitle{Another way to do spatial analysis: regressing on distance}
    
Classic example:

\textit{Atkinson, Quentin D. 2011. Phonemic diversity supports a serial founder effect model of language expansion from Africa. Science 332. 346–349} 

vs. 

\textit{Jaeger et. al. Mixed effect models for genetic and areal dependencies in linguistic typology. Linguistic Typology 15 (2011), 281–320.}

\end{frame}

\begin{frame}[fragile]\frametitle{The crucial technique: mixed-effects linear models}

We need to test whether the effect is significant when different phyla and genera are allowed to have their own slopes and intercepts:


\begin{figure}
\centerline{
\includegraphics[width=\textwidth]{distance_from_Africa_phonological_diversity.png}
}
\end{figure}

\end{frame}

\begin{frame}[fragile]\frametitle{Mixed-effects linear models in R}
    
First-class support in the package \texttt{lme4}.

\vspace{5pt}

A comprehensive introduction: Bates et al. Fitting Linear Mixed-Effects Models Using
lme4. 10.18637/jss.v067.i01. \url{https://www.jstatsoft.org/article/view/v067i01/} (or \url{https://cran.r-project.org/web/packages/lme4/vignettes/lmer.pdf})

\end{frame}

\section{Comparing different types of dissimilarities: Mantel test}

\begin{frame}[fragile]\frametitle{Looking for correlations between dissimilarities: Mantel test}
    
Mantel test is a test for significance of correlation between two dissimilarity matrices. Commonly used in life sciences:

\begin{figure}
\centerline{
\includegraphics[width=.5\textwidth]{mantel_example.png}
}
\end{figure}

{\footnotesize \url{http://www.mfe.govt.nz/publications/environmental-reporting/new-zealand-marine-environment-classification-overview/append-2}}

\end{frame}

\begin{frame}[fragile]\frametitle{Partial Mantel test}
    
Computes the correlation between two dissimilarity matrices in the presence of a control matrix.

\vspace{5pt}

{\small
``Individually, the Mantel correlation with geography for tone is r = 0.169, P = 0.015; for ASPM-D, r = 0.074, P = 1.000 (because of Holm's multiple comparisons correction; ref. 56); and for MCPH-D, r = 0.543, P < 0.001. Each of tone, ASPM-D, and MCPH-D have low but significant spatial autocorrelations (62): Moran's I (63) is 0.178, 0.164, and 0.121, and Geary's c (64) is 0.634, 0.438 and 0.718, respectively, P < 0.001 for all, suggesting that, potentially, geographical factors might explain the observed relationship. However, the (partial) Mantel correlation between tone and the pair ASPM-D/MCPH-D is r = 0.333, P < 0.001, and, when controlling for geography, it decreases only slightly and still remains highly significant, r = 0.291, P = 0.003, showing that geography is not a good explanation for our empirical findings.'' \url{http://www.pnas.org/content/104/26/10944.full}
}

\end{frame}

\begin{frame}[fragile]\frametitle{Graph}
    
\begin{figure}
\centerline{
  \includegraphics[height=.7\textheight]{tone_on_genes.jpg}
}
\end{figure}

{\footnotesize Horizontal axis---frequency of ASPM-D; vertical axis---frequency of MCPH-D. Filled squares---toneless langs; open squares---tonal langs.}

\end{frame}

\begin{frame}[fragile]\frametitle{Let's do some Mantle testing}
    
Data: consonant inventories of Eurasian languages (from EURPhon $+$ PHOIBLE).\pause

\vspace{5pt}

Distance matrices:
\begin{enumerate}
  \item Jaccard distance between inventories\pause
  \item Phylogenetic distances ($0$ if languages are from the same genus; $1$ if they are from the same phylum, but from different genera; $2$ if they are unrelated)\pause
  \item Neighbour-distances ($0$ if languages are neighbours; $1$ if they aren't; we'll define what `neighbour' means later)\pause
  \item Geodesic distances computed based on coordinates
\end{enumerate}

\end{frame}

\begin{frame}[fragile]\frametitle{The data}
    
{\footnotesize
\begin{table}[]
\centerline{
\begin{tabular}{@{}lllll@{}}
\toprule
Language pair & Neighbours & \begin{tabular}[c]{@{}l@{}}Same\\ phylum\end{tabular} & \begin{tabular}[c]{@{}l@{}}Same\\ genus\end{tabular} & \begin{tabular}[c]{@{}l@{}}Jaccard\\ distance\end{tabular} \\ \midrule
Skolt Saami vs. Tundra Yukaghir & 1 & 0 & 0 & 0.66 \\
Nganasan (Avam) vs. Skolt Saami & 1 & 1 & 0 & 0.685 \\
Nganasan (Avam) vs. Tundra Yukaghir & 1 & 0 & 0 & 0.462 \\
Sakha (Standard) vs. Tundra Yukaghir & 1 & 0 & 0 & 0.621 \\
Nganasan (Avam) vs. Sakha (Standard) & 1 & 0 & 0 & 0.71 \\
Nganasan (Avam) vs. Tundra Yukaghir & 1 & 0 & 0 & 0.462 \\
Mah Meri vs. Southern Sama & 1 & 0 & 0 & 0.484 \\
Dhivehi vs. Southern Sama & 1 & 0 & 0 & 0.706 \\
Dhivehi vs. Mah Meri & 1 & 0 & 0 & 0.733 \\ \bottomrule
\end{tabular}
}
\end{table}
}

With coordinates in a separate file. Easiest to use some Python to convert this into distance matrices.

\end{frame}

\begin{frame}[fragile]\frametitle{t-SNE}
    
Phyla are evidently important, but they do not explain everything:

\begin{figure}
\centerline{
  \includegraphics[width=1.1\textwidth]{phono_tsne_jaccard.pdf}
}
\end{figure}

\end{frame}

\begin{frame}[fragile]\frametitle{Mantel analyses}
    
{\footnotesize
\begin{verbatim}
> mantel(jaccard.dist ~ geodesic.dist)
   mantelr      pval1      pval2      pval3  llim.2.5% ulim.97.5% 
 0.2698558  0.0010000  1.0000000  0.0010000  0.2530986  0.2869117 
> mantel(jaccard.dist ~ neighbour.dist)
   mantelr      pval1      pval2      pval3  llim.2.5% ulim.97.5% 
 0.1173985  0.0010000  1.0000000  0.0010000  0.1115672  0.1225923 
> mantel(jaccard.dist ~ neighbour.dist + geodesic.dist)
   mantelr      pval1      pval2      pval3  llim.2.5% ulim.97.5% 
0.07795815 0.00100000 1.00000000 0.00100000 0.07240279 0.08385912 
> mantel(jaccard.dist ~ neighbour.dist + phylo.dist)
   mantelr      pval1      pval2      pval3  llim.2.5% ulim.97.5% 
0.07757610 0.00100000 1.00000000 0.00100000 0.07179254 0.08258140 
> mantel(jaccard.dist ~ neighbour.dist + geodesic.dist + phylo.dist)
   mantelr      pval1      pval2      pval3  llim.2.5% ulim.97.5% 
0.05501380 0.00100000 1.00000000 0.00100000 0.04976498 0.06054619 
\end{verbatim}
}
{
\scriptsize
pval1 : one-tailed p-value (null hypothesis: $r <= 0$). pval2 : one-tailed p-value (null hypothesis: $r >= 0$). pval3 : two-tailed p-value (null hypothesis: $r = 0$).
}
\end{frame}

\begin{frame}[fragile]\frametitle{Correlogram}
    
\begin{figure}
\centering
\includegraphics[height=.7\textheight]{mantel_correlogram_jaccard.pdf}
\end{figure}

{\footnotesize It's rather hard to interpret exactly, but roughly it says that phonological distances between inventories depend on geographical distances up to a certain cut-off distance, when the relationship becomes noisy.}

\end{frame}

\section{Bootstrap for significance testing of between-group differences}

\begin{frame}[fragile]\frametitle{An alternative to Mantel test}
    
There are $6$ groups in the data:

\begin{verbatim}
> table(d$same_phylum, d$same_genus, d$neighbours)
, ,  = 0
        0     1
  0 97667     0
  1 14401  1925

, ,  = 1
        0     1
  0  1134     0
  1   890   754
\end{verbatim}

We can repeatedly sample with replacement $754$ elements from each of the groups and record the median. Then we can compare \textit{bootstrap confidence intervals} of the medians with a needed significance level (usually $0.95$).

\end{frame}

\begin{frame}[fragile]\frametitle{An aside on confidence intervals}
    
A \textit{confidence interval} is interval constructed in such a way that if the procedure were repeated a large number of times, it would include the parameter of interest a particular number of times.\pause

\vspace{5pt}

It may be shown that if we sample with replacement from a given sample and then take the interval from the 0.025th quantile to the 0.975th quantile of some statistic, we will construct a confidence interval for this statistic. E.g., if we construct such a confidence interval for the median, there's 95\% chance that it includes the true median, and only 5\% chance that it doesn't. This corresponds to the usual $p$-value of $0.05$.

\end{frame}

\begin{frame}[fragile]\frametitle{Confidence intervals computed based on bootstrap replications}
    
\begin{figure}
\centering
\includegraphics[height=.8\textheight]{hist_confint.pdf}
\end{figure}

\end{frame}

\begin{frame}[fragile]\frametitle{Bootstrap confidence intervals}
    
\begin{figure}
\centerline{
\includegraphics[width=1.1\textwidth]{bootstrap_confint.pdf}
}
\end{figure}

\end{frame}

\begin{frame}[fragile]\frametitle{Bootstrap confidence intervals: code}
    
Compute medians of bootstrap samples:

{\footnotesize
\begin{verbatim}
# Neighbours, same phylum, same genus
n.sp.sg <- c()
# Neighbours, same phylum, different genus
n.sp.dg <- c()
# Neighbours, different phylum, different genus
n.dp.dg <- c()
# Not neighbours, same phylum, same genus
nn.sp.sg <- c()
# Not neighbours, same phylum, different genus
nn.sp.dg <- c()
# Not neighbours, different phylum, different genus
nn.dp.dg <- c()
# 10000 replications
for (i in 1:10^4) {
    n.sp.sg[i] <- median( sample(d$jaccard[which( (d$neighbours == 1) &
                                (d$same_phylum == 1) &
                                (d$same_genus == 1) )], 754, replace = T) )
    n.sp.dg[i] <- median( sample(d$jaccard[which( (d$neighbours == 1) &
                                (d$same_phylum == 1) &
                                (d$same_genus == 0) )], 754, replace = T) )
    # Etc. Some code omitted
}
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]\frametitle{Extract confidence intervals and plot them}
    
{\small
\begin{verbatim}
confint.df <- data.frame(
    boundaries = c(
        quantile(n.sp.sg, 0.025),
        quantile(n.sp.sg, 1-0.025),
        # Etc. come code omitted.
    ),
    groups = c(1,1,2,2,3,3,4,4,5,5,6,6)
)
plot(boundaries~groups, data = confint.df, pch = 16, col = 'red',
     xlab = 'Groups: (+n,+p,+g), (+n,+p,-g), (+n,-p,-g),
     (-n,+p,+g), (-n,+p,-g), (-n,-p,-g)',
     ylab = 'Confidence intervals', cex = 1.2, cex.lab = 1.6)
odd_ids = seq(1, nrow(confint.df), 2)
even_ids = seq(2, nrow(confint.df), 2)
segments(confint.df$groups[odd_ids], confint.df$boundaries[odd_ids],
         confint.df$groups[even_ids], confint.df$boundaries[even_ids],
         lwd = 1.5)
abline(h = confint.df$boundaries, col = 'grey', lty = 2)
\end{verbatim}
}

\end{frame}

\section{Neighbour graphs} % (fold)

\begin{frame}[fragile]\frametitle{The easy way}
    
Count languages as neighbours if they are separated by not more than 1000 kilometers.\pause

\vspace{5pt}

PRO: very easy to calculate.\pause

\vspace{5pt}

CONTRA: may introduce spurious neighbours.

\end{frame}

\begin{frame}[fragile]\frametitle{An alternative---Delaunay triangulation}
    
For a set of points on the plane, the Delaunay triangulation connects points into triangles in such a way that no point of the set is inside the circumcircle of any triangle.

\begin{figure}
\centering
\includegraphics[height=.6\textheight]{Delaunay_circumcircles.png}
\end{figure}

\end{frame}

\begin{frame}[fragile]\frametitle{Some complications (with solutions)}
    
\begin{enumerate}
  \item Earth is noth a plane, but an ellipsoid. However, it is also easy to compute a Delaunay triangulation for points on a sphere.\pause
  \item Delaunay triangulation obligatorily connects points on the peryphery (constructs convex hull of the points). However, we can throw away all the edges longer than, e.g., $1000$ kilometers.
\end{enumerate}

\end{frame}

\begin{frame}[fragile]\frametitle{Delaunay triangulation of Eurasian languages}

\begin{figure}
\centerline{
  \includegraphics[width=1.1\textwidth]{neighbour_graph.png}
}
\end{figure}

\end{frame}

\begin{frame}[fragile]\frametitle{Delaunay triangulation: Python code}
{\small
\begin{verbatim}
# Given that we already have the arrays `lats', `lons', and `names'
import numpy as np
from scipy.spatial import ConvexHull
points_cartesian = np.ndarray((0, 3))
for i in range(len(lats)):
    lat = np.deg2rad(np.float(lats[i]))
    lon = np.deg2rad(np.float(lons[i]))
    x = np.cos(lat) * np.cos(lon)
    y = np.cos(lat) * np.sin(lon)
    z = np.sin(lat)
    points_cartesian = np.vstack((points_cartesian, (x, y, z)))
qhull = ConvexHull(points_cartesian) # <---
# The data on the edges of the triangulation
# is contained in qhull.simplices
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]\frametitle{Extracting data from a qhull object}

{\small
\begin{verbatim}
# One option: build a real graph
import networkx as nx
from itertools import combinations
from geopy.distance import VincentyDistance
lang_graph = nx.Graph()
# Or just store the edges in a matrix
incidence_matrix = np.zeroes(c(qhull.simplices.shape[0],
                               qhull.simplices.shape[0]))
for i in range(qhull.simplices.shape[0]):
    for pair in combinations(qhull.simplices[i], 2):
        p = pair[0]; q = pair[1]
        lat1, lon1 = lats[p], lons[p]
        lat2, lon2 = lats[q], lons[q]
        d = VincentyDistance((lat1, lon1),
                             (lat2, lon2)).kilometers
        if d < 1000:
            # Graph option
            lang_graph.add_edge(p, q)
            # Matrix option
            incidence_matrix[p,q] = incidence_matrix[q,p] = 1
\end{verbatim}
}


\end{frame}

\end{document}