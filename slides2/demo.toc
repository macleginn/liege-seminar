\beamer@sectionintoc {1}{Different ways of doing spatial analysis}{3}{0}{1}
\beamer@sectionintoc {2}{Comparing macro-regions}{7}{0}{2}
\beamer@sectionintoc {3}{Incorporating distances}{52}{0}{3}
\beamer@sectionintoc {4}{Comparing different types of dissimilarities: Mantel test}{55}{0}{4}
\beamer@sectionintoc {5}{Bootstrap for significance testing of between-group differences}{67}{0}{5}
\beamer@sectionintoc {6}{Neighbour graphs}{74}{0}{6}
