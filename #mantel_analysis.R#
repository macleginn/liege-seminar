Sys.setlocale("LC_ALL", 'en_US.UTF-8')

library(cluster)
library(ecodist)
library(Rtsne)

img.dir <- '/home/macleginn/Teaching/Liege seminar, May 2018/slides2/'

coords <- read.csv('all_data_coords_dedup.csv', h = T)

jaccard.dist <- as.dist(read.csv('jaccard_dist_mat.csv', h = T, row.names = 1))
jacc.tsne <- Rtsne(jaccard.dist)

cairo_pdf(paste0(img.dir, 'phono_tsne_jaccard.pdf'), width = 16, height = 10)
plot(jacc.tsne$Y[,1], jacc.tsne$Y[,2], pch = 16, col = coords$Phylum,
     xlab = '', ylab = '', xlim = c(-22, 17))
legend('topleft', levels(coords$Phylum),
       col = seq_along(levels(coords$Phylum)),
       pch = 16)
dev.off()

geodesic.dist <- as.dist(read.csv('geodesic_dist_mat.csv', h = T, row.names = 1))
neighbour.dist <- as.dist(read.csv('neighbour_dist_mat.csv', h = T, row.names = 1))
phylo.dist <- as.dist(read.csv('phylo_dist_mat.csv', h = T, row.names = 1))

# Check for correlations
jaccard <- c()
geodesic <- c()
neigh <- c()
phylo <- c()
j.m <- as.matrix(jaccard.dist)
g.m <- as.matrix(geodesic.dist)
n.m <- as.matrix(neighbour.dist)
p.m <- as.matrix(phylo.dist)
for (i in 2:(ncol(j.m)-1)) {
    for (j in (i+1):ncol(j.m)) {
        jaccard <- c(jaccard, j.m[i,j])
        geodesic <- c(geodesic, g.m[i,j])
        neigh <- c(neigh, n.m[i,j])
        phylo <- c(phylo, p.m[i,j])
    }
}
all.dist.df <- data.frame(jaccard = jaccard, geodesic = geodesic, neigh = neigh, phylo = phylo)
pairs(all.dist.df)

# Sanity check
mantel.test <- mantel(geodesic.dist, neighbour.dist)

mantel.test$signif

mantel.geo.jacc <- mantel(geodesic.dist, jaccard.dist)

mantel.geo.jacc$signif

mantel.neigh.jacc <- mantel(neighbour.dist, jaccard.dist)

mantel.neigh.jacc$signif

mantel.neigh.jacc.phylo <- mantel.partial(neighbour.dist, jaccard.dist, phylo.dist, na.rm = T)

mantel.neigh.jacc.phylo

table(d$same_phylum, d$same_genus, d$neighbours)
## , ,  = 0

   
##         0     1
##   0 97667     0
##   1 14401  1925

## , ,  = 1

   
##         0     1
##   0  1134     0
##   1   890   754

# Neighbours, same phylum, same genus
n.sp.sg <- c()
# Neighbours, same phylum, different genus
n.sp.dg <- c()
# Neighbours, different phylum, different genus
n.dp.dg <- c()
# Not neighbours, same phylum, same genus
nn.sp.sg <- c()
# Not neighbours, same phylum, different genus
nn.sp.dg <- c()
# Not neighbours, different phylum, different genus
nn.dp.dg <- c()
# 10000 replications
for (i in 1:10^4) {
    n.sp.sg[i] <- median( sample(d$jaccard[which( (d$neighbours == 1) & (d$same_phylum == 1) & (d$same_genus == 1) )], 754, replace = T) )
    n.sp.dg[i] <- median( sample(d$jaccard[which( (d$neighbours == 1) & (d$same_phylum == 1) & (d$same_genus == 0) )], 754, replace = T) )
    n.dp.dg[i] <- median( sample(d$jaccard[which( (d$neighbours == 1) & (d$same_phylum == 0) & (d$same_genus == 0) )], 754, replace = T) )
    nn.sp.sg[i] <- median( sample(d$jaccard[which( (d$neighbours == 0) & (d$same_phylum == 1) & (d$same_genus == 1) )], 754, replace = T) )
    nn.sp.dg[i] <- median( sample(d$jaccard[which( (d$neighbours == 0) & (d$same_phylum == 1) & (d$same_genus == 0) )], 754, replace = T) )
    nn.dp.dg[i] <- median( sample(d$jaccard[which( (d$neighbours == 0) & (d$same_phylum == 0) & (d$same_genus == 0) )], 754, replace = T) )
}

confint.df <- data.frame(
    boundaries = c(
        quantile(n.sp.sg, 0.025),
        quantile(n.sp.sg, 1-0.025),
        quantile(n.sp.dg, 0.025),
        quantile(n.sp.dg, 1-0.025),
        quantile(n.dp.dg, 0.025),
        quantile(n.dp.dg, 1-0.025),
        quantile(nn.sp.sg, 0.025),
        quantile(nn.sp.sg, 1-0.025),
        quantile(nn.sp.dg, 0.025),
        quantile(nn.sp.dg, 1-0.025),
        quantile(nn.dp.dg, 0.025),
        quantile(nn.dp.dg, 1-0.025)
    ),
    groups = c(1,1,2,2,3,3,4,4,5,5,6,6)
)

cairo_pdf(paste0(img.dir, 'hist_confint.pdf'), width = 10, height = 16)
par(mfrow = c(2,1))
hist(n.sp.dg, breaks = 30, xlim = c(0.60, 0.75), main = '+n,+p,-g', xlab = '')
abline(v = quantile(n.sp.dg, c(0.025, 1-0.025)), col = 'red', lty = 2)
hist(nn.sp.dg, breaks = 30, xlim = c(0.60, 0.75), main = '-n,+p,-g', xlab = '')
abline(v = quantile(nn.sp.dg, c(0.025, 1-0.025)), col = 'red', lty = 2)
dev.off()

cairo_pdf(paste0(img.dir, 'bootstrap_confint.pdf'), width = 16, height = 10)
par(mar = c(6,6,2,2))
plot(boundaries~groups, data = confint.df, pch = 16, col = 'red',
     xlab = 'Groups: (+n,+p,+g), (+n,+p,-g), (+n,-p,-g), (-n,+p,+g), (-n,+p,-g), (-n,-p,-g)',
     ylab = 'Confidence intervals', cex = 1.2, cex.lab = 1.6)
odd_ids = seq(1, nrow(confint.df), 2)
even_ids = seq(2, nrow(confint.df), 2)
segments(confint.df$groups[odd_ids], confint.df$boundaries[odd_ids],
         confint.df$groups[even_ids], confint.df$boundaries[even_ids],
         lwd = 1.5)
abline(h = confint.df$boundaries, col = 'grey', lty = 2)
dev.off()
